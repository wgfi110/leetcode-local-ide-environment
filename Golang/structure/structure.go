package structure

type ListNode struct {
	val  int
	next *ListNode
}

type TreeNode struct {
	val   int
	left  *TreeNode
	right *TreeNode
}
